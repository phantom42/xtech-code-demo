<?php include('inc/autoLoader.php') ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Joe Coleman - Programming Test</title>

    <?php include('inc/bootstrapHead.php'); ?>
  </head>
  <body>
    <?php include('inc/navbar.php') ?>

    <div class="container">

        <h1>XTech HQ Programming Test</h1>
        <p class="lead">Just an example of a basic form.</p>
        
        <?php include('inc/results.php')?>
        <?php include('inc/subjectForm.php')?>
        <?php include('inc/fileDisplay.php')?>

    
    </div> <!-- /container -->

    <?php include('inc/bootstrapJS.php') ; ?>
    <script src="js/subjectForm.js"></script>
  </body>
</html>
