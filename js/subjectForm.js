$(function() {
    //attach events 
    $('#employerRadio').on('click',toggleForm) ;
    $('#employeeRadio').on('click',toggleForm) ;
    $('#fileListLauncher').on('click',reloadFileList) ;
    
    //custom functions for radio buttons
    function toggleForm(){
        var selectedVal = $('input:radio[name=personType]:checked').val() ;
        if (selectedVal === 'employee') {
            $('#employerDiv').hide() ;
            $('#employeeDiv').show() ;
        } else {
            $('#employeeDiv').hide() ;
            $('#employerDiv').show() ;
        }
    }
    
    // function to refresh file list
    function reloadFileList() {
        $('#fileModalBody').load('fileModal.php');
    }
    
    // client side form validation
    function validateForm() {
        var vFirstName = $.trim($("#firstName").val()) ;
        var vLastName = $.trim($("#lastName").val()) ;
        var vEmailAddress = $.trim($("#emailAddress").val());
        var vCompanyName = $.trim($("#companyName").val()) ;
        var vEmployerName = $.trim($("#employerName").val());
        var validated = true ;
        
        var errorString = '' ;
        
        if (vFirstName.length === 0) {
            errorString += '<li>First Name must be filled out</li>' ;
        }
        if (vLastName.length === 0) {
            errorString += '<li>Last Name must be filled out</li>' ;
        }
        if (vEmailAddress.length === 0) {
            errorString += '<li>Email Address must be filled out</li>' ;
        }
        if (vCompanyName.length === 0 || vEmployerName.length === 0) {
            if ($('input:radio[name=personType]:checked').val() === 'employee') {
                if (vEmployerName.length === 0) {
                    errorString += '<li>You must select the company you work for</li>' ;
                }
            } else {
                if (vCompanyName.length === 0) {
                    errorString += '<li>You must enter in your company name</li>' ;
                }
                
            }
        }
        
        if (errorString.length) {
            errorString = '<ul>' + errorString ;
            errorString += '</ul>' ;
            displayErrors(errorString) ;
            validated = false ;
        }
        return validated ;
    }
    
    function displayErrors(errorString) {
        var resultDisplay = $("#resultDisplay");
        resultDisplay.html('<div class="alert alert-warning">' + errorString + '</div>');
        resultDisplay.fadeIn() ;
    }
    
    // submit function for form
    $("#subjectForm").submit(function(e){
        var f = $("#subjectForm") ;
        var postData = $(this).serialize() ;
        var formAction = f.attr('action');
        var resultDisplay = $("#resultDisplay");
        var formDisplay = $("#subjectForm");
        
        var isValid = validateForm() ;
        
        if (isValid === false) {
            return false ;
        } 
        resultDisplay.hide() ;
        e.preventDefault() ;
        
        $.ajax(
        {
            url : formAction,
            type: "POST",
            data : postData,
            success:function(resultData)
            {
                resultDisplay.html(resultData['message']);
                //responseDisplay.fadeIn();
                if (resultData['result'] === true) {
                    formDisplay.fadeOut();
                }
                resultDisplay.fadeIn();
            },
            error: function(err)
            {
                //console.log('failure');
                resultDisplay.html('<div class="alert alert-danger">What did you do, Ray?</div>');
                resultDisplay.fadeIn();
                
            }
        });
        return true ;
    });
    
    // set default displays for items on the screen
    toggleForm();
    $("#resultDisplay").hide();
});