<?php
    class fileio {
        public $dataDir = 'data' ;
        
        public function setDir($newDir) {
            $this->dataDir = $newDir ;
        }
        
        public function getDir() {
            return $this->dataDir . "<br/>" ;
        }
        
        public function insertSubject($personData) {
            // this handles inserts and updates of data to the files
            // in a real db situation, we would check against an id and insert/update appropriately
            try {
                // generate the file name
                $fileName = $this->dataDir . '/' . $personData['employerName']  . '.txt';
                // convert the associative array to something writeable
                $dataWrite = http_build_query($personData,'',',' ) . PHP_EOL ;
                $writeResult = file_put_contents($fileName, $dataWrite, FILE_APPEND ) ;
                $result = ($writeResult > 0) ? true : false ;
            } catch (Exception $e) {
                $result = false ;
            }
            
            return $result;
            
        }
        public function renderEmployerList() {
            $returnString = '';
            if ($handle = opendir($this->dataDir)) {
                while (false !== ($entry = readdir($handle))) {
                    // loop through everything that isnt a blank file name or folder navigation links
                    if ($entry != "." && $entry != ".." && strlen($entry) > 4) {
                        $dataArray = explode('.',$entry) ;
                        $returnString .= "<option>$dataArray[0]</option>";
                    }
                }
                closedir($handle);
            }
            return $returnString ;
        }
        public function renderFileNames() {
            $returnString = '<ol>';
            if ($handle = opendir($this->dataDir)) {
                while (false !== ($entry = readdir($handle))) {
                    // loop through everything that isnt a blank file name or folder navigation links
                    if ($entry != '.' && $entry != '..' && strlen($entry) > 4) {
                        // the first part of the filename is the company name
                        $dataArray = explode('.',$entry) ;
                        $fileLoc = $this->dataDir . '/' . $entry ;
                        $returnString .= '<li><a href=" ' . $fileLoc . '" target="_blank">'. $dataArray[0] . '</a></li>';
                    }
                }
                closedir($handle);
            }
            $returnString .= '</ol>';
            return $returnString ;
        }
    }
?>