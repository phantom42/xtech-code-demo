
        <?php 
            include_once('inc/autoLoader.php');
            $fObj = new fileio ;
        ?>
        <form role="form" id="subjectForm" class="subjectForm" action="processSubject.php">
            <div class="form-group">
                <label for="firstName">First Name</label>
                <input type="text" class="form-control" id="firstName" name="firstName" placeholder="First Name">
            </div>
            <div class="form-group">
                <label for="lastName">Last Name</label>
                <input type="text" class="form-control" id="lastName" name="lastName" placeholder="Last Name">
            </div>
            <div class="form-group">
                <label for="emailAddress">Email Address</label>
                <input type="text" class="form-control" id="emailAddress" name="emailAddress" placeholder="Email Address">
            </div>
            <div class="form-group">
                <label for="personType">Category</label>
                <div class="radio">
                    <label>
                        <input type="radio" name="personType" id="employerRadio" value="employer" checked>
                         Employer
                     </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="personType" id="employeeRadio" value="employee">
                        Employee
                    </label>
                </div>
            </div>
            <div class="form-group" id="employerDiv">
                <label for="companyName">Company</label>
                <input type="text" class="form-control" id="companyName" name="companyName" placeholder="Company Name">
            </div>
            <div class="form-group" id="employeeDiv">
                <label for="employerName">Company</label>
                <select class="form-control" id="employerName" name="employerName">
                     <?php 
                        echo $fObj->renderEmployerList() 
                    ?>
                </select>
                
            </div>
            <button type="reset" class="btn btn-default">Reset</button>
            <button type="submit" class="btn btn-default">Submit</button>
        </form>
        
        