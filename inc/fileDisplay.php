    <div>&nbsp;</div>
    <div class="form-group">
        
            <button id="fileListLauncher" class="btn btn-default" data-toggle="modal" data-target="#fileList">View Existing Files</button>
    </div>
<div class="modal fade" id="fileList" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Existing Data Files</h4>
      </div>
      <div class="modal-body" id="fileModalBody">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>