<?php
    include('inc/autoLoader.php');
    
    $fObj = new fileio ;
    $validForm = true ;
    $message = '' ;
    
    // since we are only writing to a file, we don't need to worry about sanitization too much
    // for now, let's just escape the special characters 
    $personData = array (
        'firstName'     => htmlspecialchars(trim($_POST['firstName']),ENT_QUOTES),
        'lastName'      => htmlspecialchars(trim($_POST['lastName']),ENT_QUOTES),
        'emailAddress'  => htmlspecialchars(trim($_POST['emailAddress']),ENT_QUOTES)
        );
    
    if ($_POST['personType'] === 'employee') {
        // if subject is an employee, look at the employerName field
        $personData['employerName'] = $_POST['employerName'] ;
    } else {
        // if subject is an employer, look at the companyName field
        $personData['employerName'] = $_POST['companyName'] ;
    };
    
    // never ever trust client side validation to have run. them clients are shady characters.
    if (strlen($personData['firstName']) === 0) {
        $message .= '<li>First Name must be filled out</li>' ;
    }
    if (strlen($personData['lastName']) === 0) {
        $message .= '<li>Last Name must be filled out</li>' ;
    }
    if (strlen($personData['emailAddress']) === 0) {
        $message .= '<li>Email Address must be filled out</li>' ;
    }
    if (strlen($personData['employerName']) === 0) {
        if ($personData['personType'] === 'employee') {
            $message .= '<li>You must select the company you work for</li>' ;
        } else {
            $message .= '<li>You must enter in your company name</li>' ;
        }
    }
    
    
    if (strlen($message)) {
        $message = '<ul>' . $message ;
        $message .= '</ul>' ;
        $validForm = false ;
    }
    
    if ($validForm) {
        $success = $fObj->insertSubject($personData) ;
    } else {
        $success = false ;
    }
    
    // set up a basic result array with a message to display to the user
    $result = array(
        'result'        => $success,
        'message'       => '<div class="alert alert-success">Congratulations. The data has been added.</div>',
        'personData'    => $personData // not necessary, but good for debugging. would remove this in prod.
    );
    
    // if the write was a failure, then change the message to the error message
    if ($success === false) {
        $result['message'] = '<div class="alert alert-warning">'. $message .'</div>' ;
    } 
    
    // now convert all this to json and send it back... (to the future!)
    header("content-type:application/json");
    echo json_encode($result);
 
?>